package br.com.flexait.core;

import java.util.List;

public interface SessionComponent {

	public abstract boolean isLogged();

	public abstract void logout();

	public abstract void logon(User user);

	public abstract User getUser();
	
	public abstract List<Breadcrumb> getBreadcrumbs();
}