package br.com.flexait.core;

public interface User {

	Long getId();
	
	String getEmail();

	String getName();
	
}
