package br.com.flexait.core.interceptor;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.interceptor.TypeNameExtractor;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.serialization.ProxyInitializer;
import br.com.caelum.vraptor.serialization.SerializerBuilder;
import br.com.caelum.vraptor.serialization.xstream.XStreamBuilder;
import br.com.caelum.vraptor.serialization.xstream.XStreamJSONSerialization;
import br.com.caelum.vraptor.serialization.xstream.XStreamSerializer;

@Component
public class LazyJSONSerialization extends XStreamJSONSerialization {

	private SerializerHolder holder;

	public LazyJSONSerialization(SerializerHolder holder,
			HttpServletResponse response, TypeNameExtractor extractor,
			ProxyInitializer initializer, XStreamBuilder builder) {
		super(response, extractor, initializer, builder);
		this.holder = holder;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected SerializerBuilder getSerializer() {
		return new XStreamSerializer(getXStream(), holder.createWriter(),
				extractor, initializer);
	}
}