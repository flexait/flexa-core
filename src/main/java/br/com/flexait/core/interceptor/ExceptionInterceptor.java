package br.com.flexait.core.interceptor;

import static br.com.caelum.vraptor.view.Results.http;

import javax.inject.Inject;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.view.Results;
import br.com.flexait.core.component.RequestComponent;

@Intercepts
public class ExceptionInterceptor implements Interceptor {
	
	private final Result result;
	private final RequestComponent request;

	@Inject
	public ExceptionInterceptor(Result result, RequestComponent request) {
		this.result = result;
		this.request = request;
	}

	@Override
	public void intercept(InterceptorStack stack, ResourceMethod method,
			Object resourceInstance) throws InterceptionException {

		try {
			stack.next(method, resourceInstance);
		} catch (Exception e) {
			if(request.isAjax()) {
				result.use(http()).body(getMessage(e)).setStatusCode(400);
				e.printStackTrace();
			}
			else {
				result.include("error", getRootCause(e));
				
				try {
					result.use(Results.referer()).redirect();
				} catch (IllegalStateException e1) {
					result.redirectTo(request.getContextPath());
				}
			}
		}
	}
	
	private Throwable getRootCause(Throwable throwable) {
		if (throwable.getCause() != null
				&& throwable.getCause().getMessage() != null)
			return getRootCause(throwable.getCause());

		return throwable;
	}

	private String getMessage(Exception e) {
		return String
				.format("{\"errors\":[{\"message\":\"%s\", \"category\":\"ajax\"}]}", getRootCause(e))
				.replace("\n", "");
	}

	@Override
	public boolean accepts(ResourceMethod method) {
		return true;
	}

}