package br.com.flexait.core.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.plugin.hibernate4.HibernateTransactionInterceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;

@Intercepts(before = {HibernateTransactionInterceptor.class}, after = {ExceptionInterceptor.class})
public class LazySerializerInterceptor implements Interceptor {

	private SerializerHolder holder;
	private HttpServletResponse response;

	public LazySerializerInterceptor(SerializerHolder holder,
			HttpServletResponse response) {
		this.holder = holder;
		this.response = response;
	}

	public boolean accepts(ResourceMethod method) {
		return true;
	}

	public void intercept(InterceptorStack stack, ResourceMethod method,
			Object instance) {
		stack.next(method, instance);
		if (holder.getWriter() != null) {
			try {
				response.getWriter().print(holder.getWriter());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}