package br.com.flexait.core.interceptor;

import javax.servlet.http.HttpServletResponse;

import br.com.caelum.vraptor.interceptor.TypeNameExtractor;
import br.com.caelum.vraptor.serialization.ProxyInitializer;
import br.com.caelum.vraptor.serialization.SerializerBuilder;
import br.com.caelum.vraptor.serialization.gson.GsonJSONSerialization;
import br.com.caelum.vraptor.serialization.gson.GsonSerializer;
import br.com.caelum.vraptor.serialization.gson.VRaptorGsonBuilder;
import br.com.caelum.vraptor.serialization.xstream.Serializee;

public class LazyGsonSerialization extends GsonJSONSerialization {

	private SerializerHolder holder;

	public LazyGsonSerialization(HttpServletResponse response,
			TypeNameExtractor extractor, ProxyInitializer initializer,
			VRaptorGsonBuilder builder, Serializee serializee, SerializerHolder holder) {
		super(response, extractor, initializer, builder, serializee);
		this.holder = holder;
	}
	
	@Override
	protected SerializerBuilder getSerializer() {
		return new GsonSerializer(builder, holder.createWriter(), extractor, initializer, serializee);
	}

}
