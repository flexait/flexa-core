package br.com.flexait.core.interceptor;

import java.io.StringWriter;
import java.io.Writer;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class SerializerHolder {
	private StringWriter writer;

	public Writer createWriter() {
		this.writer = new StringWriter();
		return this.writer;
	}

	public Writer getWriter() {
		return this.writer;
	}
}