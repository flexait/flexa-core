package br.com.flexait.core;

public interface Breadcrumb {
	Long getId();
	
	String getName();
	
	Long getNivel();
}
