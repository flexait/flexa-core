/**
 * 
 */
package br.com.flexait.core;

import java.io.Serializable;

/**
 * @author flexa
 *
 */
public interface IModel extends Serializable{
	
	public Long getId();
	public void setId(Long id);
	public String getLabel();

}
