package br.com.flexait.core.validation;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.View;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.validator.Message;
import br.com.caelum.vraptor.validator.Validations;

@Component
public class ValidatorUtil {
	
	private Validator validator;

	@Inject
	public ValidatorUtil(Validator validator) {
		this.validator = validator;
	}

	public Validator validator() {
		return validator;
	}

	public void validate(Object object, Class<?>... groups) {
		validator.validate(object, groups);
		validator.onErrorSendBadRequest();
	}
	
	public void validate(Object object) {
		validator.validate(object);
		validator.onErrorSendBadRequest();
	}

	public void checking(Validations rules) {
		validator.checking(rules);
		validator.onErrorSendBadRequest();
	}

	public void validateProperties(Object object, String... properties) {
		validator.validateProperties(object, properties);
	}

	public void validateProperty(Object object, String property,
			Class<?>... groups) {
		validator.validateProperty(object, property, groups);
	}

	public <T extends View> T onErrorUse(Class<T> view) {
		return validator.onErrorUse(view);
	}

	public void addAll(Collection<? extends Message> message) {
		validator.addAll(message);
	}

	public void add(Message message) {
		validator.add(message);
	}

	public List<Message> getErrors() {
		return validator.getErrors();
	}

	public boolean hasErrors() {
		return validator.hasErrors();
	}

	public <T> T onErrorForwardTo(Class<T> controller) {
		return validator.onErrorForwardTo(controller);
	}

	public <T> T onErrorForwardTo(T controller) {
		return validator.onErrorForwardTo(controller);
	}

	public <T> T onErrorRedirectTo(Class<T> controller) {
		return validator.onErrorRedirectTo(controller);
	}

	public <T> T onErrorRedirectTo(T controller) {
		return validator.onErrorRedirectTo(controller);
	}

	public <T> T onErrorUsePageOf(Class<T> controller) {
		return validator.onErrorUsePageOf(controller);
	}

	public <T> T onErrorUsePageOf(T controller) {
		return validator.onErrorUsePageOf(controller);
	}

	public void onErrorSendBadRequest() {
		validator.onErrorSendBadRequest();
	}
}
