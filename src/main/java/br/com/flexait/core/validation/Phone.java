package br.com.flexait.core.validation;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = {})
@Documented
@Pattern(regexp = "^\\(\\d{2}\\)\\d{8,9}$", message = "{invalidPhone}")
public @interface Phone {
	
	String message() default "{invalidPhone}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
}