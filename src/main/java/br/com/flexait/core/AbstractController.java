package br.com.flexait.core;

import static br.com.caelum.vraptor.view.Results.json;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.notNullValue;

import org.hibernate.criterion.Order;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.serialization.JSONSerialization;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.validator.Validations;
import br.com.flexait.core.component.ComponentWrapper;
import br.com.flexait.core.validation.ValidatorUtil;

public abstract class AbstractController<T extends IModel> {

	public static final String CONTROLLER = "controller";
	public static final String SESSION = "session";
	private AbstractDao<T> dao;
	private Result result;
	
	public AbstractController(Result result, AbstractDao<T> dao) {
		this.result = result;
		this.dao = dao;
		result().include(SESSION, getSessionComponent());
		result().include(CONTROLLER, getClass());
	}

	private SessionComponent getSessionComponent() {
		if(component() == null) {
			return null;
		}
		return component().sessionComponent();
	}

	protected Result result() {
		return result;
	}
	
	protected JSONSerialization resultJson() {
		return result().use(json());
	}
	
	protected ValidatorUtil validator() {
		return component().validator();
	}

	protected ComponentWrapper component() {
		return dao.component();
	}
	
	protected SessionComponent sessionComponent() {
		return getSessionComponent();
	}
	
	protected String[] getIncludes() {
		return new String[0];
	}
	
	protected String[] getExcludes() {
		return new String[0];
	}
	
	protected Order getDefaultOrder() {
		return Order.asc("id");
	}
	
	protected T save(final T model, Class<?>... groups) throws Exception {
		T saved = dao().saveOrUpdate(model, groups);
		serialize(saved);
		return saved;
	}
	
	protected T save(final T model) throws Exception {
		T saved = dao().saveOrUpdate(model);
		serialize(saved);
		return saved;
	}

	protected void validate(final T model) {
		validator().checking(new Validations() {
			{
				that(model, notNullValue(), "objeto", "objeto.notNull");
			}
		});
		validator().validate(model);
		validator().onErrorSendBadRequest();
	}
	
	protected void checkId(final IModel model) {
		validator().checking(new Validations() {
			{
				if (that(model, notNullValue(), "model", "model.notNull")) {
					that(model.getId(), notNullValue(), "model.id", "model.id.notNull");
					that(model.getId(), greaterThan(0L), "model.id", "model.id.notBeZero");
				}
			}
		});
		validator().onErrorSendBadRequest();		
	}

	protected void serialize(Object obj) {
		result().use(json()).withoutRoot().from(obj)
		.include(getIncludes()).exclude(getExcludes())
		.serialize();
	}
	
	protected void serializeWithoutIncludes(Object obj) {
		result().use(json()).withoutRoot().from(obj).serialize();
	}
	
	protected void success() {
		String message = "ok";
		serializeWithoutIncludes(message);
	}
	
	protected AbstractDao<T> dao() {
		return dao;
	}
	
	protected IModel newInstanceModel() throws InstantiationException,
			IllegalAccessException {
		return (IModel) dao().getDomainClass().newInstance();
	}

	protected IModel newInstanceModelId(long id) {
		IModel model = null;
		try {
			model = newInstanceModel();
			model.setId(id);
		} catch (InstantiationException | IllegalAccessException e) {
			validator().add(new ValidationMessage(e.getMessage(), "exception"));
			validator().onErrorSendBadRequest();
		}
		return model;
	}

}
