package br.com.flexait.core;

import java.util.List;

import org.hibernate.Criteria;

import com.google.common.base.Strings;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;

public abstract class RestReadController<T extends IModel> extends
		AbstractController<T> {

	public RestReadController(Result result, AbstractDao<T> dao) {
		super(result, dao);
	}
	
	@Get("/")
	public void index() throws Exception {
	}
	
	@Get("/list.json")
	public void list(String q, Integer count, Integer page) throws Exception {
		if(Strings.isNullOrEmpty(q)) {
			q = "%";
		}
		List<T> list = dao().search(q, count, page);
		serialize(list);
	}
	
	protected void listByCriteria(String q, Integer count, Integer page, Criteria criteria) throws Exception {
		if(Strings.isNullOrEmpty(q)) {
			q = "%";
		}
		List<T> list = dao().search(q, count, page, criteria);
		serialize(list);
	}
	
	@Get("/count")
	public void count(String q) {
		long count = dao().searchCount(q);
		resultJson().from(count).serialize();
	}
	
	protected void countByCriteria(String q, Criteria criteria) {
		long count = dao().searchCount(q, criteria);
		resultJson().from(count).serialize();
	}	
	
	@Get("/searchCount")
	public void searchCount() {
		long count = dao().count();
		serializeWithoutIncludes(count);
	}

	@Get("/listsimple.json")
	public void listJson() throws Exception {
		List<Json> list = dao().listJson(getDefaultOrder());
		serializeWithoutIncludes(list);
	}

	@Get("/{id}")
	public void get(long id) throws Exception {
		T obj = dao().getById(id);
		serialize(obj);
	}
	
	@Path("/:action/:id")
	public void url() {}

}