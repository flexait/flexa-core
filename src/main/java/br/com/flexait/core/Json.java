package br.com.flexait.core;


public class Json implements IModel {

	private static final long serialVersionUID = -1801217356243493618L;

	private String label;

	private Long id;
	
	public Json(Long id, String label) {
		this.id = id;
		this.label = label;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String getLabel() {
		return label;
	}

	public static <T> Json of(T t) {
		IModel m = (IModel) t;
		return new Json(m.getId(), m.getLabel());
	}
}