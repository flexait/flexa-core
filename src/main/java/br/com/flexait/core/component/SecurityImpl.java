package br.com.flexait.core.component;

import org.apache.commons.codec.digest.DigestUtils;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class SecurityImpl implements Security {

	@Override
	public String sha1(String string) {
		return DigestUtils.sha1Hex(string);
	}

}