package br.com.flexait.core.component;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class RequestComponentImpl implements RequestComponent {

	private final HttpServletRequest request;

	@Inject
	public RequestComponentImpl(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public boolean isAjax() {
		String header = request.getHeader("X-Requested-With");
		String content = request.getHeader("content-type");
		String accept = request.getHeader("accept");
		return (header != null && header.equals("XMLHttpRequest")) ||
				(content != null && content.contains("application/json")) ||
				(accept != null && accept.contains("application/json"));
	}

	@Override
	public String getContextPath() {
		return request.getContextPath();
	}
}
