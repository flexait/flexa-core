package br.com.flexait.core.component;

public interface RequestComponent {

	boolean isAjax();

	String getContextPath();

}
