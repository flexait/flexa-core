package br.com.flexait.core.component;

import java.io.Serializable;

import javax.inject.Inject;

import org.hibernate.Session;

import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.ioc.Component;
import br.com.flexait.core.SessionComponent;
import br.com.flexait.core.validation.ValidatorUtil;

@Component
public class ComponentWrapper {

	private Session session;
	private SessionComponent sessionComponent;
	private ValidatorUtil validator;
	private Environment env;

	@Inject
	public ComponentWrapper(Session session, SessionComponent sessionComponent,
			ValidatorUtil validator, Environment env) {
		this.session = session;
		this.sessionComponent = sessionComponent;
		this.validator = validator;
		this.env = env;
	}

	public Session session() {
		return session;
	}

	public SessionComponent sessionComponent() {
		return sessionComponent;
	}

	public ValidatorUtil validator() {
		return validator;
	}

	public void delete(Object arg0) {
		session().delete(arg0);
	}

	public Object load(Class<?> arg0, Serializable arg1) {
		return session().load(arg0, arg1);
	}

	public Object merge(Object arg0) {
		return session().merge(arg0);
	}

	public Environment env() {
		return env;
	}

}