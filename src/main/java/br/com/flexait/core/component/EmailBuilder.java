package br.com.flexait.core.component;

import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.com.caelum.vraptor.ioc.Component;
import br.com.flexait.core.User;

@Component
public class EmailBuilder {
	
	private final HtmlEmail email;
	
	public EmailBuilder() throws EmailException {
		email = new HtmlEmail();
		email.setCharset(EmailConstants.ISO_8859_1);
		email.setFrom("indica@sorricred.com.br", "Sorricred");
	}

	public EmailBuilder toUser(User user) throws EmailException {
		email.addTo(user.getEmail(), user.getName());
		return this;
	}

	public HtmlEmail build() {
		return email;
	}

	public EmailBuilder withMessage(String message) throws EmailException {
		email.setHtmlMsg(message);
		email.setMsg(message);
		return this;
	}

	public EmailBuilder withSubject(String subject) {
		email.setSubject(subject);
		return this;
	}
	
}
