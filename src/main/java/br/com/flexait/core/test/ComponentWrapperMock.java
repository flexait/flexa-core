package br.com.flexait.core.test;

import br.com.flexait.core.User;
import br.com.flexait.core.component.ComponentWrapper;


public class ComponentWrapperMock extends ComponentWrapper {
	
	public ComponentWrapperMock() {
		super(null, new SessionComponentMock(), new ValidatorUtilMock(), null);
	}
	
	public ComponentWrapperMock(User user) {
		super(null, new SessionComponentMock(user), new ValidatorUtilMock(), null);
	}
	
}
