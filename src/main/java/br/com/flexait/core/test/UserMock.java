package br.com.flexait.core.test;

import br.com.flexait.core.User;

public class UserMock implements User {

	@Override
	public Long getId() {
		return 1L;
	}

	@Override
	public String getEmail() {
		return "email@email.com";
	}

	@Override
	public String getName() {
		return "user";
	}

}
