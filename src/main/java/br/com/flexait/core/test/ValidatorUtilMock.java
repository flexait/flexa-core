package br.com.flexait.core.test;

import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.util.test.JSR303MockValidator;
import br.com.flexait.core.validation.ValidatorUtil;

public class ValidatorUtilMock extends ValidatorUtil {

	public ValidatorUtilMock() {
		super(new JSR303MockValidator());
	}
	
	public ValidatorUtilMock(Validator validator) {
		super(validator);
	}

}
