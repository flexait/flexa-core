package br.com.flexait.core.test;

import java.util.List;

import br.com.flexait.core.Breadcrumb;
import br.com.flexait.core.SessionComponent;
import br.com.flexait.core.User;

public class SessionComponentMock implements SessionComponent {

	private User user;

	public SessionComponentMock() {
		this.logon(new UserMock());
	}
	
	public SessionComponentMock(User user) {
		this.logon(user);
	}
	
	@Override
	public boolean isLogged() {
		return user != null;
	}

	@Override
	public void logout() {
		user = null;
	}

	@Override
	public void logon(User user) {
		this.user = user;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public List<Breadcrumb> getBreadcrumbs() {
		return null;
	}

}
