package br.com.flexait.core.utils;

public class ConvertersUtil {
	public static Integer parseInt(String value) {
		try {
			return Integer.parseInt(value);
		}catch(Exception e) {
			return null;
		}
	}
	
	public static Long parseLong(String value) {
		try {
			return Long.parseLong(value);
		}catch(Exception e) {
			return null;
		}
	}

	public static Double parseDouble(String valor) {
		try {
			return Double.parseDouble(valor);
		}catch(Exception e) {
			return null;
		}
	}
}
