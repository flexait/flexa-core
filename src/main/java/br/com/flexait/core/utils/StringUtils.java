package br.com.flexait.core.utils;

import com.google.common.base.Strings;

public class StringUtils {

	public String stripHtml(String string) {
		if(Strings.isNullOrEmpty(string)) {
			return string;
		}
		return string.replaceAll("\\<.*?\\>", "");
	}

}
