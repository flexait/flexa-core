package br.com.flexait.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.flexait.core.component.SecurityImpl;

public class DateUtil {

	private static final String DATE_TIME_MASK = "dd/MM/yyyy HH:mm";
	private static final String DATE_TIME_MASK_FRONT = "dd/MM/yyyy • HH:mm";
	private static final String DATE_TIME_ACESSO_MASK = "dd/MM/yyyy 'às' HH:mm'h'";
	private static final String TIME_MASK = "HH:mm:ss";
	private static final String DATE_MASK = "dd/MM/yyyy";
	private static final String MONTH_YEAR_MASK = "MM/yyyy";
	private static final long DAY = 24 * 60 * 60 * 1000;
	
	public static Calendar addDays(final Calendar from, final int count) {
		return daysOperation(from, count, true);
	}
	
	public static Calendar removeDays(final Calendar from, final int count) {
		return daysOperation(from, count, false);
	}
	
	public static String formatDate(Calendar calendar) {
		if(calendar == null) {
			return null;
		}
		
		SimpleDateFormat simpleDateFormat;
		
		if (calendar.get(Calendar.HOUR) == 0 
				&& calendar.get(Calendar.MINUTE) == 0 
				&& calendar.get(Calendar.SECOND) == 0){
			
			simpleDateFormat = new SimpleDateFormat(DATE_MASK);		
		}else{
			simpleDateFormat = new SimpleDateFormat(DATE_TIME_MASK);
		}
				
		return simpleDateFormat.format(calendar.getTime());
	}
	
	public static String formatDateTime(Calendar calendar) {
		if(calendar == null) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_MASK);		
		return simpleDateFormat.format(calendar.getTime());
	}
	
	public static String formatDateTimeFront(Calendar calendar) {
		if(calendar == null) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_MASK_FRONT);		
		return simpleDateFormat.format(calendar.getTime());
	}
	
	public static String formatDateTimeAcesso(Calendar calendar) {
                if(calendar == null) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_ACESSO_MASK);		
		return simpleDateFormat.format(calendar.getTime());
	}
	
	private static String getMask(String value) {
		switch (value.length()) {
		case 7:
			return MONTH_YEAR_MASK;

		case 10:
			return DATE_MASK;
		
		default:
			return DATE_TIME_MASK;
		}
	}
	
	public static Calendar parseDate(String date, String mask) {
		SimpleDateFormat sdf = new SimpleDateFormat(mask);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(date.trim()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}
	
	public static Calendar parseDate(String value) {
		return parseDate(value, getMask(value.trim()));
	}
	
	public static int getDaysAfterToday(Calendar date) {
		if(date == null) {
			return 0;
		}
		return (int) ((Calendar.getInstance().getTimeInMillis() - date.getTimeInMillis()) / DAY);
	}
	
	private static Calendar daysOperation(final Calendar from, final int count, final boolean isIncrement) {
	    for (int daysBack = 0; daysBack < count; ++daysBack) {
	        do {
	            from.add(Calendar.DAY_OF_YEAR, (isIncrement ? 1 : -1));
	        } while(isWeekend(from));
	    }
	    return from;
	}
	
	private static boolean isWeekend(Calendar cal) {
	    return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
	           cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}
	
	public static int millisToDay(long millis) {
		return (int)(millis / DAY);
	}
	
	public static String getSaudacao(){
		Calendar c1 = Calendar.getInstance();
		int hora = c1.get(Calendar.HOUR_OF_DAY);
		
		if(hora < 12){
			return "Bom dia";
		}
		else if (hora > 12 && hora < 18)
		{ 
			return "Boa Tarde";
		}
		else {
			 return "Boa Noite";
		}
	}
	
	public static String formatTime(Calendar calendar) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_MASK);		
		return simpleDateFormat.format(calendar.getTime());
	}
	
	public static String getClock(int seconds) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.add(Calendar.SECOND, seconds);
		
		return formatTime(calendar);
	}
	
	public static Calendar parseDateToCalendar(Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);		
		
		return calendar;
	}

	public Calendar addMonth(Calendar date, int quant) {
		Calendar instance = Calendar.getInstance();
		instance.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH));
		instance.set(Calendar.MONTH, date.get(Calendar.MONTH));
		instance.set(Calendar.YEAR, date.get(Calendar.YEAR));
		instance.add(Calendar.MONTH, quant);
		return instance;
	}
	
	public static int getDaysDateMaiorBeforeDateMenor(Calendar dateMaior, Calendar dateMenor) {
		if(dateMaior == null || dateMenor == null) {
			return 0;
		}
		return (int) ((dateMaior.getTimeInMillis() - dateMenor.getTimeInMillis()) / DAY);
	}	
	
	public static int getMesNormalizado(Calendar date){
		if (date == null){
			return 0;
		}
		return date.get(Calendar.MONTH) + 1;
	}
	
	public static int getAno(Calendar date){
		if (date == null){
			return 0;
		}
		return date.get(Calendar.YEAR);
	}

	public static String hash(Calendar date) {
		String sha1 = new SecurityImpl().sha1(formatDateTime(date));
		return sha1.substring(0, 7);
	}

}
