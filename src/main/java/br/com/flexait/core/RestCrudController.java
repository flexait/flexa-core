package br.com.flexait.core;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;

public abstract class RestCrudController<T extends IModel> extends
		RestReadController<T> {
	
	public RestCrudController(Result result, AbstractDao<T> dao) {
		super(result, dao);
	}

	@Post("/add")
	@Consumes("application/json")
	public T add(T model) throws Exception {
		return save(model);
	}

	@Put("/{model.id}")
	@Consumes("application/json")
	public T edit(T model) throws Exception {
		checkId(model);
		return save(model);
	}

	@SuppressWarnings("unchecked")
	@Delete("/{id}")
	public void remove(long id) throws Exception {
		IModel imodel = newInstanceModelId(id);
		T model = (T) imodel;
		checkId(model);

		dao().remove(model);
		success();
	}

}