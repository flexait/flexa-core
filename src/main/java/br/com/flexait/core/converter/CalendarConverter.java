package br.com.flexait.core.converter;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;
import br.com.caelum.vraptor.ioc.Component;
import br.com.flexait.core.utils.DateUtil;

import com.google.common.base.Strings;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.thoughtworks.xstream.converters.SingleValueConverter;
import com.thoughtworks.xstream.converters.SingleValueConverterWrapper;

@Component
@Convert(Calendar.class)
public class CalendarConverter extends SingleValueConverterWrapper implements JsonSerializer<Calendar>, JsonDeserializer<Calendar>, Converter<Calendar>{

	public CalendarConverter(SingleValueConverter wrapped) {
		super(wrapped);
	}

	@Override
	public JsonElement serialize(Calendar src, Type typeOfSrc,
			JsonSerializationContext context) {
		String formatDate = DateUtil.formatDate(src);
		return new JsonPrimitive(formatDate);
	}

	@Override
	public Calendar deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		return DateUtil.parseDate(json.getAsString());
	}

	@Override
	public String toString(Object obj) {
		Calendar calendar = (Calendar) obj;
		int hour = calendar.get(Calendar.HOUR);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);
		
		/** verificando se o formato é date */
		if(hour == 0 && min == 0 && sec == 0) {
			return DateUtil.formatDate(calendar);
		}
		return DateUtil.formatDateTime(calendar);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class type) {
		return Calendar.class.isAssignableFrom(type) || GregorianCalendar.class.isAssignableFrom(type);
	}

	@Override
	public Calendar convert(String value, Class<? extends Calendar> type,
			ResourceBundle bundle) {
		
		return parse(value);
	}

	private Calendar parse(String value) {
		if(Strings.isNullOrEmpty(value)) {
			return null;
		}
		
		return DateUtil.parseDate(value);
	}
}