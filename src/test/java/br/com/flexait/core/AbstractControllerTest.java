package br.com.flexait.core;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.ValidationException;
import br.com.flexait.core.component.ComponentWrapper;
import br.com.flexait.core.test.ComponentWrapperMock;

public class AbstractControllerTest {

	AbstractController<MyModel> controller;
	@Mock private Result result;
	private MyDao dao;
	
	static class MyModel extends Model {
		private static final long serialVersionUID = 6441015582108148234L;
		String name;
		public MyModel(){}
	}
	
	class MyController extends AbstractController<MyModel> {
		public MyController(Result result, MyDao dao) {
			super(result, dao);
		}
	}
	
	class MyDao extends AbstractDao<MyModel> {
		private static final long serialVersionUID = 1812460866878906728L;
		public MyDao(ComponentWrapper component) {
			super(component);
		}
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		dao = new MyDao(new ComponentWrapperMock());
		controller = new MyController(result, dao);
	}

	@Test
	public void shouldInstanceNewModelClass() throws InstantiationException, IllegalAccessException {
		assertThat(controller.newInstanceModel(), instanceOf(IModel.class));
	}
	
	@Test
	public void shouldReturnModelWithSettedId() throws InstantiationException, IllegalAccessException {
		IModel model = controller.newInstanceModelId(1L);
		assertThat(model.getId(), equalTo(1L));
	}
	
	@Test(expected = ValidationException.class)
	public void shouldDispatchValidationErrorOnInstanciateError() throws InstantiationException, IllegalAccessException {
		controller = spy(controller);
		when(controller.newInstanceModel()).thenThrow(new InstantiationException());
		controller.newInstanceModelId(1L);
	}

}
