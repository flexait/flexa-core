package br.com.flexait.core;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.util.test.MockSerializationResult;
import br.com.caelum.vraptor.validator.ValidationException;
import br.com.flexait.core.component.ComponentWrapper;
import br.com.flexait.core.test.ComponentWrapperMock;

public class RestCrudControllerTest {
	
	RestCrudController<MyModel> controller;
	private MockSerializationResult result;
	private AbstractDao<MyModel> dao;
	private MyModel model;
	private ComponentWrapper component;
	
	static class MyModel extends Model {
		private static final long serialVersionUID = -1905196748674554604L;
		String name;
	}
	
	class MyController extends RestCrudController<MyModel> {
		public MyController(Result result, AbstractDao<MyModel> dao) {
			super(result, dao);
		}
	}

	class MyDao extends AbstractDao<MyModel> {
		private static final long serialVersionUID = 1812460866878906728L;
		public MyDao(ComponentWrapper component) {
			super(component);
		}
	}
	
	@Before
	public void setUp() throws Exception {
		model = new MyModel();
		model.setId(1L);
		component = new ComponentWrapperMock();
		dao = spy(new MyDao(component));
		result = new MockSerializationResult();
		controller = new MyController(result, dao);
	}

	@Test
	public void shouldIncludeOkOnRemove() throws Exception {
		controller = spy(controller);
		doNothing().when(controller).checkId(any(MyModel.class));
		doNothing().when(dao).remove(any(MyModel.class));
		controller.remove(1L);
		
		assertThat(result.serializedResult(), containsString("ok"));
		verify(dao).remove(any(MyModel.class));
	}
	
	@Test(expected = ValidationException.class)
	public void shouldDispatchValidationErrorOnDeleteNull() throws Exception {
		controller = spy(controller);
		doNothing().when(dao).remove(any(MyModel.class));
		
		controller.remove(0);
	}

}
