package br.com.flexait.core.test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UserMockTest {

	UserMock user;
	
	@Before
	public void setUp() throws Exception {
		user = new UserMock();
	}

	@Test
	public void shouldSetData() {
		assertThat(user.getId(), equalTo(1L));
		assertThat(user.getName(), equalTo("user"));
		assertThat(user.getEmail(), equalTo("email@email.com"));
	}

}