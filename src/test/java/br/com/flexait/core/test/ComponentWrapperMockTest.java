package br.com.flexait.core.test;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import br.com.flexait.core.test.ComponentWrapperMock;
import br.com.flexait.core.test.ValidatorUtilMock;

public class ComponentWrapperMockTest {

	ComponentWrapperMock component;
	
	@Before
	public void setUp() throws Exception {
		component = new ComponentWrapperMock();
	}

	@Test
	public void shouldReturnValidationUtilMock() {
		assertThat(component.validator(), instanceOf(ValidatorUtilMock.class));
	}

}