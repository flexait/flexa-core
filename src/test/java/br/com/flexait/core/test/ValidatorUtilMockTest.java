package br.com.flexait.core.test;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidatorUtilMockTest {

	ValidatorUtilMock mock;
	
	@Before
	public void setUp() throws Exception {
		mock = new ValidatorUtilMock();
	}

	@Test
	public void shouldSetDep() {
		assertThat(mock.validator(), notNullValue());
	}

}
