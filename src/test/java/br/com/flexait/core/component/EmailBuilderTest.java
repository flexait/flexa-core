package br.com.flexait.core.component;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import javax.mail.MessagingException;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Before;
import org.junit.Test;

import br.com.flexait.core.User;
import br.com.flexait.core.test.UserMock;

public class EmailBuilderTest {

	EmailBuilder builder;
	private User user;
	
	@Before
	public void setUp() throws Exception {
		builder = new EmailBuilder();
		user = new UserMock();
	}
	
	@Test
	public void shouldSetDefaultParametersOnConstruct() {
		HtmlEmail email = builder.build();
		assertThat(email.getFromAddress().getAddress(), equalTo("indica@sorricred.com.br"));
	}

	@Test
	public void shouldSetUserToEmail() throws EmailException {
		Email email = builder.toUser(user).build();
		assertThat(email.getToAddresses().get(0).getAddress(), equalTo("email@email.com"));
		assertThat(email.getToAddresses().get(0).getPersonal(), equalTo("user"));
	}
	
	@Test
	public void shouldSetMessage() throws IOException, MessagingException, EmailException {
		builder.withMessage("message").build();
	}
	
	@Test(expected = EmailException.class)
	public void shouldDispatchSetMessage() throws IOException, MessagingException, EmailException {
		builder.withMessage(null).build();
	}
	
	@Test
	public void shouldSetSubject() {
		HtmlEmail email = builder.withSubject("subject").build();
		assertThat(email.getSubject(), equalTo("subject"));
	}
	
}
