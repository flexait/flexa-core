package br.com.flexait.core.component;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import br.com.flexait.core.test.SessionComponentMock;
import br.com.flexait.core.test.UserMock;

public class SessionComponentMockTest {

	SessionComponentMock mock;
	
	@Before
	public void setUp() throws Exception {
		mock = new SessionComponentMock();
	}

	@Test
	public void shouldReturnUser() {
		assertThat(mock.getUser(), notNullValue());
	}
	
	@Test
	public void shouldReturnTrueIfLogged() {
		assertThat(mock.isLogged(), equalTo(true));
	}
	
	@Test
	public void shouldReturnFalseIfLogout() {
		mock.logout();
		assertThat(mock.isLogged(), equalTo(false));
	}
	
	@Test
	public void shouldSetNewUser() {
		mock.logon(new UserMock());
		assertThat(mock.getUser().getId(), equalTo(1L));
	}

}
