package br.com.flexait.core.component;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class RequestComponentImplTest {

	RequestComponent component;
	@Mock private HttpServletRequest request;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		component = new RequestComponentImpl(request);
	}

	@Test
	public void shouldReturnTrueIfRequestIsAjax() {
		when(request.getHeader("X-Requested-With")).thenReturn("XMLHttpRequest");
		
		assertThat(component.isAjax(), equalTo(true));
	}
	
	@Test
	public void shouldReturnFalseIfHeaderIsNull() {
		when(request.getHeader("X-Requested-With")).thenReturn(null);
		
		assertThat(component.isAjax(), equalTo(false));
	}

	@Test
	public void shouldReturnFalseIfHeaderReturn() {
		when(request.getHeader("X-Requested-With")).thenReturn("POST");
		
		assertThat(component.isAjax(), equalTo(false));
	}

}
