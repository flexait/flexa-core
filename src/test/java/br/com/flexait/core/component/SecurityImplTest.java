package br.com.flexait.core.component;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SecurityImplTest {

	Security security;
	
	@Before
	public void setUp() throws Exception {
		security = new SecurityImpl();
	}

	@Test
	public void shouldHashSha1Data() {
		assertThat(security.sha1("123"), equalTo("40bd001563085fc35165329ea1ff5c5ecbdbbeef"));
	}

}