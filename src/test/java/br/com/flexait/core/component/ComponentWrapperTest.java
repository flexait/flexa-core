package br.com.flexait.core.component;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.flexait.core.SessionComponent;
import br.com.flexait.core.validation.ValidatorUtil;

public class ComponentWrapperTest {

	ComponentWrapper wrapper;
	@Mock private Session session;
	@Mock private SessionComponent sessionComponent;
	@Mock ValidatorUtil validator;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		wrapper = new ComponentWrapper(session, sessionComponent, validator, null);
	}

	@Test
	public void shouldSetDependencies() {
		assertThat(wrapper.session(), notNullValue());
		assertThat(wrapper.sessionComponent(), notNullValue());
		assertThat(wrapper.validator(), notNullValue());
	}

}