package br.com.flexait.core.interceptor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.util.test.MockResult;
import br.com.flexait.core.component.RequestComponent;

public class ExceptionInterceptorTest {

	private ExceptionInterceptor interceptor;
	@Mock private InterceptorStack stack;
	@Mock private ResourceMethod method;
	@Mock private Object resourceInstance;
	private MockResult result;
	@Mock private RequestComponent request;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		result = new MockResult();
		interceptor = new ExceptionInterceptor(result, request);
	}

	@Test
	public void sholdAcceptTrue() {
		assertThat(interceptor.accepts(null), equalTo(true));
	}
	
	@Test
	public void shouldExecuteNext() {
		doNothing().when(stack).next(method, resourceInstance);
		
		interceptor.intercept(stack, method, resourceInstance);
		
		verify(stack).next(method, resourceInstance);
	}
	
	@Test
	public void shouldIncludeErrorAjaxIfHappenTransactionError() throws Exception {
		when(request.isAjax()).thenReturn(true);
		
		doThrow(new HibernateException("error")).when(stack).next(method, resourceInstance);
		
		interceptor.intercept(stack, method, resourceInstance);
		
		assertThat(result.used(), equalTo(true));
	}
	
	@Test
	public void shouldIncludeErrorServletIfHappenTransactionError() throws Exception {
		when(request.isAjax()).thenReturn(false);
		
		doThrow(new HibernateException("error")).when(stack).next(method, resourceInstance);
		
		interceptor.intercept(stack, method, resourceInstance);
		
		assertThat(((HibernateException)result.included().get("error")).getMessage(), equalTo("error"));
	}

}
