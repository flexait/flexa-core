package br.com.flexait.core;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.flexait.core.component.ComponentWrapper;

public class AbstractDaoTest {

	private AbstractDao<TestModel> dao;
	@Mock private ComponentWrapper component;

	class TestModel extends Model {
		private static final long serialVersionUID = 2596598316226410126L;
		String name;
	}
	
	class TestDao extends AbstractDao<TestModel> {
		private static final long serialVersionUID = -1595346302123326935L;
		public TestDao(ComponentWrapper component) {
			super(component);
		}
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		dao = new TestDao(component);
	}

	@Test
	public void shouldConfigureDependencies() {
		assertThat(dao.component(), notNullValue());
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void shouldExtractDomainClassByType() {
		assertThat(dao.getDomainClass(), equalTo((Class)TestModel.class));
	}
	
}
