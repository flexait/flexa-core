package br.com.flexait.core.validation;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.validation.constraints.NotNull;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.util.test.JSR303MockValidator;
import br.com.caelum.vraptor.validator.ValidationException;
import br.com.flexait.core.validation.ValidatorUtil;

public class ValidatorUtilTest {

	ValidatorUtil util;
	private Validator mockValidator;	
	
	@Before
	public void setUp() throws Exception {
		mockValidator = new JSR303MockValidator();
		util = new ValidatorUtil(mockValidator);
	}

	@Test
	public void shouldInjectDep() {
		assertThat(util.validator(), equalTo(mockValidator));
	}
	
	class Bean {
		@NotNull
		String name;
	}
	
	@Test(expected = ValidationException.class)
	public void shouldBeInvalideBean() {
		Bean bean = new Bean();
		util.validate(bean);
	}
	
	@Test
	public void shouldBeValideBean() {
		Bean bean = new Bean();
		bean.name = "Test";
		util.validate(bean);
	}

}
