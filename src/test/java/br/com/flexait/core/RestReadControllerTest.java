package br.com.flexait.core;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.util.test.MockSerializationResult;
import br.com.flexait.core.test.ComponentWrapperMock;

public class RestReadControllerTest {

	RestReadController<MyModel> rest;
	@Mock private AbstractDao<MyModel> dao;
	private MockSerializationResult result;
	
	class MyModel extends Model {
		private static final long serialVersionUID = 3695667806367784135L;
		private String name;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	class MyController extends RestReadController<MyModel> {
		public MyController(Result result, AbstractDao<MyModel> dao) {
			super(result, dao);
		}
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		result = new MockSerializationResult();
		
		MyModel my = new MyModel();
		my.setName("bar");
		
		MyModel myModel = new MyModel();
		myModel.name = "foo";
		
		List<MyModel> asList = Arrays.asList(my, myModel);
		when(dao.search("", 0, 0)).thenReturn(asList);
		when(dao.component()).thenReturn(new ComponentWrapperMock());
		
		rest = new MyController(result, dao);
	}

	@Test
	public void shouldIncludeObjectInList() throws Exception {
		rest.count("");
		
		assertThat(result.serializedResult(), containsString("long"));
		verify(dao).searchCount("");
	}

}
