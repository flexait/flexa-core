package br.com.flexait.core.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringUtilsTest {

	StringUtils util;
	
	@Before
	public void setUp() throws Exception {
		util = new StringUtils();
	}

	@Test
	public void shouldStripHtml() {
		String striped = util.stripHtml("<b>string</b>");
		assertThat(striped, equalTo("string"));
	}
	
	@Test
	public void shouldReturnNullIfHtmlIsNull() {
		String striped = util.stripHtml(null);
		assertThat(striped, nullValue());
	}

}
